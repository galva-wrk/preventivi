--
-- File generated with SQLiteStudio v3.2.1 on ven gen 4 11:29:40 2019
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: clienti
DROP TABLE IF EXISTS clienti;

CREATE TABLE clienti (
    id        INTEGER       PRIMARY KEY ASC AUTOINCREMENT
                            NOT NULL,
    nome      VARCHAR (50)  DEFAULT ""
                            NOT NULL,
    cognome   VARCHAR (50)  DEFAULT ""
                            NOT NULL,
    ragSoc    VARCHAR (100) DEFAULT ""
                            NOT NULL,
    cdFis     CHAR (16)     DEFAULT ""
                            NOT NULL,
    pIva      CHAR (11)     DEFAULT ""
                            NOT NULL,
    indirizzo VARCHAR (50)  DEFAULT ""
                            NOT NULL,
    numCiv    VARCHAR (10)  DEFAULT ""
                            NOT NULL,
    cap       CHAR (5)      NOT NULL
                            DEFAULT 0,
    comune    VARCHAR (100) NOT NULL
                            DEFAULT "",
    provincia CHAR (2)      NOT NULL
                            DEFAULT "",
    tipoCli   BOOLEAN       NOT NULL
                            DEFAULT (1),
    dtNasc    DATE          NOT NULL
                            DEFAULT (1 - 1 - 1),
    luogoNasc VARCHAR (100) NOT NULL
                            DEFAULT ""
);


-- Table: opere
DROP TABLE IF EXISTS opere;

CREATE TABLE opere (
    id          INTEGER         PRIMARY KEY ASC AUTOINCREMENT
                                NOT NULL,
    opera       VARCHAR (50)    NOT NULL
                                DEFAULT "",
    descrizione VARCHAR (2000)  NOT NULL
                                DEFAULT "",
    importo     DECIMAL (10, 2) NOT NULL
                                DEFAULT (0) 
);


-- Table: opereDescr
DROP TABLE IF EXISTS opereDescr;

CREATE TABLE opereDescr (
    id          INTEGER        PRIMARY KEY AUTOINCREMENT
                               NOT NULL,
    idOpera     INTEGER        NOT NULL,
    progressivo INT (2)        NOT NULL,
    descrizione VARCHAR (2000) NOT NULL
);


-- Table: opereDescrPreventivi
DROP TABLE IF EXISTS opereDescrPreventivi;

CREATE TABLE opereDescrPreventivi (
    id          INTEGER        PRIMARY KEY AUTOINCREMENT
                               NOT NULL,
    numPrev     INT (9)        NOT NULL,
    idOpera     INTEGER        NOT NULL,
    progressivo INT (2)        NOT NULL,
    descrizione VARCHAR (2000) NOT NULL
);


-- Table: operePreventivi
DROP TABLE IF EXISTS operePreventivi;

CREATE TABLE operePreventivi (
    idOpera     INTEGER         PRIMARY KEY ASC AUTOINCREMENT
                                NOT NULL,
    id          INTEGER         NOT NULL,
    opera       VARCHAR (50)    NOT NULL
                                DEFAULT "",
    descrizione VARCHAR (2000)  DEFAULT "",
    importo     DECIMAL (10, 2) NOT NULL
                                DEFAULT (0),
    numPrev     INT (9)         NOT NULL,
    idOperaKey  INTEGER         NOT NULL
                                DEFAULT (0) 
);


-- Table: parametri
DROP TABLE IF EXISTS parametri;

CREATE TABLE parametri (
    id          INTEGER    PRIMARY KEY AUTOINCREMENT,
    codice      CHAR (10)  NOT NULL,
    descrizione CHAR (50)  NOT NULL,
    valore      CHAR (200) NOT NULL
);


-- Table: preventivi
DROP TABLE IF EXISTS preventivi;

CREATE TABLE preventivi (
    numPrev       INT (9)     PRIMARY KEY
                              UNIQUE
                              NOT NULL,
    anno          INT (4)     NOT NULL,
    numero        INT (5)     NOT NULL,
    cliente       INTEGER     NOT NULL,
    daFatt        INT (10, 2) NOT NULL
                              DEFAULT (0),
    cassaGeomPerc INT (3, 2)  NOT NULL
                              DEFAULT (0),
    cassaGeom     INT (10, 2) NOT NULL
                              DEFAULT (0),
    impon         INT (10, 2) NOT NULL
                              DEFAULT (0),
    ivaPerc       INT (3, 2)  NOT NULL
                              DEFAULT (0),
    iva           INT (10, 2) NOT NULL
                              DEFAULT (0),
    art15         INT (10, 2) NOT NULL
                              DEFAULT (0),
    totFatt       INT (10, 2) NOT NULL
                              DEFAULT (0),
    swRitenuta    CHAR (1)    NOT NULL
                              DEFAULT ('S'),
    ritenutaPerc  INT (3, 2)  NOT NULL
                              DEFAULT (0),
    ritenuta      INT (10, 2) NOT NULL
                              DEFAULT (0),
    impDaCorr     INT (10, 2) NOT NULL
                              DEFAULT (0),
    dataIns       DATE        NOT NULL,
    dataMod       DATE        NOT NULL,
    swConsensi    CHAR (1)    NOT NULL
                              DEFAULT ('S'),
    swInfo        CHAR (1)    NOT NULL
                              DEFAULT ('S'),
    tipoDoc       CHAR (200)  NOT NULL
                              DEFAULT ('') 
);


-- View: clientiPreventivi_view
DROP VIEW IF EXISTS clientiPreventivi_view;
CREATE VIEW clientiPreventivi_view AS
    SELECT CASE WHEN tipoCli = 'P' THEN cognome || ' ' || nome WHEN tipoCli = 'S' THEN ragSoc END AS cliente,
           id,
           tipoCli
      FROM clienti
     WHERE id IN (
               SELECT DISTINCT cliente
                 FROM preventivi
           )
     ORDER BY tipoCli,
              cliente;


-- View: operePreventivi_view
DROP VIEW IF EXISTS operePreventivi_view;
CREATE VIEW operePreventivi_view AS
    SELECT DISTINCT id,
                    opera
      FROM opere
     WHERE id IN (
               SELECT id
                 FROM operePreventivi
           )
     ORDER BY opera;


-- View: statistiche_view
DROP VIEW IF EXISTS statistiche_view;
CREATE VIEW statistiche_view AS
    SELECT round(sum(daFatt), 2) AS daFatt,
           round(sum(totFatt), 2) AS totFatt,
           substr(dataMod, 1, 4) AS anno,
           substr(dataMod, 6, 2) AS mese
      FROM preventivi
     WHERE CAST (substr(dataMod, 1, 4) AS INTEGER) >= (
                                                          SELECT max(substr(dataMod, 1, 4) ) - 3
                                                            FROM preventivi
                                                      )
     GROUP BY substr(dataMod, 1, 7);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
